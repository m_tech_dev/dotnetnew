﻿using System;
using System.Text;
using Lab11;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Collections;
using Geometry;

namespace work1
{

    enum Figures { Circle, Rect }

    struct Fdata
    {
        public int x0,y0;
        public int color;
        public Figures type;
        public int a;
        public int b;
        public int r;
    }

    class Program
    {
        static void Main(string[] args)
        {
            //sFdata fd = new Fdata() { x0=5,y0=10,color=0x2727FF,type=Figures.Circle,a=3,b=5,r=10 };
            //Console.WriteLine(fd.color + " " + fd.type + " " + fd.x0 + " " + fd.y0);

            //Console.WriteLine(ffactorial(5));

            //Console.WriteLine(area(fd));
            
            //Circle classCircle = new Circle();
            
            //classCircle.x1 = 0;
            //classCircle.y1 = 0;
            //classCircle.move(10,15); 
            
            //Console.WriteLine(classCircle.ToString());

            //Circle[] data = new[] {classCircle};

            // void print ()
            // {
            //     foreach (var item in data)
            //     {
            //         Console.WriteLine($"x_:{item.x1} y_:{item.y1}");
            //     }    
            // }

            //print();

            //Console.WriteLine(classCircle.shapeareacircle(Figures.Circle, 4, 5, 10));
            
            // if (factorial (10, out long val))
            // {
            //     Console.WriteLine(val);
            // }
                
            //string jsonString;

            //Books books = new Books();

            //jsonString = JsonSerializer.Serialize(books);

            ///Console.WriteLine(jsonString);

            //var books1 = JsonSerializer.Deserialize<Book[]>(jsonString);

            //foreach (Book book in books.GetByPrice())       Console.WriteLine(book);
            //foreach (Book book in books.GetReverseEnum())   Console.WriteLine(book);
            //foreach (Book book in books.GetByAuthor())      Console.WriteLine(book);

            //Console.WriteLine();
            //foreach (Book book in books)        Console.WriteLine(book);

            // ArrayList books = new ArrayList();

            // books.Add(new Book(){Id=1, Price=32, Author="Seemann", Title="Dependency Injection in .NET" });
            // books.Add(new Book() {Id=2, Price=42, Author="Richter", Title="CLR via C#" });
            // books.Add(new Book() {Id=3, Price=21, Author="Munro", Title="ASP.NET MVC 5" });
            // books.Add(new Book() {Id=4, Price =39, Author="Esposito", Title= "Microsoft® ASP.NET and AJAX" });
            // books.Add(new Book() {Id=5, Price=30, Author="Esposito", Title="Architecting Applications for the Enterprise" });
            // books.Add(new Book() {Id=6, Price=23, Author="Watson", Title="Writing High-Performance .NET Code" });
            // books.Add(new Book() {Id=7, Price=37, Author="Sharp", Title="Microsoft Visual C# 2013" });
            // books.Add(new Book() {Id=8, Price=20, Author="Esposito", Title= "Programming Microsoft® ASP.NET 2.0" });
            // books.Add(new Book() {Id=9, Price=11, Author="Stubblebine", Title="Regular Expression" });
            // books.Add(new Book() {Id=10, Price=27, Author="Liberty", Title="Learning Visual Basic .Net" });
            // books.Add(new Book() {Id=11, Price=62, Author="Cwalina", Title="Framework Design Guidelines" });
            // books.Add(new Book() {Id=12, Price=55, Author="Blewett", Title="Pro Asynchronous Programming with .NET" });
            // books.Add(new Book() {Id=13, Price=47, Author="Nathan", Title="Windows Presentation Foundation Unleashed" });

            // books.Sort(Book.ByAuthor());
            // foreach (var book in books)     Console.WriteLine(book);

            Circle c1 = new Circle(new Fdata() { x0 = 1, y0 = 1, a = 2 });
            Circle c2 = new Circle(new Fdata() { x0 = 1, y0 = 1, a = 2 });

            if (c1 == c2)   Console.WriteLine("Equal");
            else            Console.WriteLine("Not Equal");        

        }
    }
}
