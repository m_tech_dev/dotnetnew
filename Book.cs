﻿using System;
using System.Collections;

namespace Lab11
{
    class Book : IComparable
    {
        public int    Id     { get; set; }
        public double Price  { get; set; }
        public string Title  { get; set; }
        public string Author { get; set; }

        public override string ToString()
        {
            return "id="+Id+" price="+Price+"$  "+Author+" : "+Title;
        }

        public int CompareTo(object obj)
        {
            Book b = obj as Book;
            
            return this.Price.CompareTo(b.Price);
        }

        public static IComparer ByAuthor()
        {
            return new SortByAuthor(); 
        }

        class SortByAuthor : IComparer
        {
            public int Compare(object x, object y)
            {
                int  res = 0;
	            Book  b1 = (Book) x;
	            Book  b2 = (Book) y;

                res = b1.Author.CompareTo(b2.Author);
                
                return res;
            }
        }

    }
}
