﻿using System;
using work1;

namespace Geometry
{
    class Circle : Shape
    {
        public int r
        {
            get { return fd.a; }
            set { fd.a=fd.b=value; }
        }
        public Circle(Fdata fd) : base(fd)
        {
            base.fd.b = base.fd.a;
            base.fd.type = Figures.Circle;
        }

    }
}

