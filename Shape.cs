﻿using System;
using work1;

namespace Geometry
{
    abstract class Shape
    {
        protected Fdata fd;

        public Shape(Fdata fd)
        {
            this.fd = fd;
        }

        public static bool operator == (Shape sh1, Shape sh2)
        {
            if (System.Object.ReferenceEquals(sh1, sh2)) return true;
            
            if ((object)sh1 == null || (object)sh2 == null) return false;

            if (sh1.GetType() != sh2.GetType()) return false;

            return sh1.fd.Equals(sh2.fd);

        }

        public static bool operator != (Shape sh1, Shape sh2)
        {
            return !(sh1 == sh2);
        }
    }
}
